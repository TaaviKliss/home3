import java.util.LinkedList;

public class LongStack {

	private LinkedList<Long> magasin; // EELNEVALT: public

	LongStack() {
		// TODO!!! Your constructor here!

		magasin = new LinkedList(); // EELNEVALT: magasin = new LinkedList<>();
	}

	public static void main(String[] argum) {
		// TODO!!! Your tests here!

		long test = interpret("2 +");

		System.out.println(test);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO!!! Your code here!

		LongStack uusMagasin = new LongStack();

		uusMagasin.magasin = (LinkedList<Long>) magasin.clone();

		return uusMagasin;
	}

	public boolean stEmpty() {
		// TODO!!! Your code here!

		return magasin.isEmpty();
	}

	public void push(long a) {
		// TODO!!! Your code here!

		magasin.push(a);
	}

	public long pop() {
		// TODO!!! Your code here!

		if (magasin.size() < 1) {

			throw new RuntimeException("Avaldis on tühi ehk puuduvad nii arvud kui ka märgid (POP meetod).");
		}

		return magasin.pop(); // EELNEVALT: oli ainult return magasin.pop();
	} // pop

	public void op(String s) {
		// TODO!!!
		if (magasin.size() < 1)

			throw new RuntimeException("Avaldis on tühi, et puuduvad nii arvud kui ka märgid (OP meetod).");

		char op = s.charAt(0);

		Long arv1 = magasin.remove(1);

		Long arv2 = magasin.remove(0);

		switch (op) {

		case '+':

			push(arv1 + arv2);
			break;

		case '-':

			push(arv1 - arv2);
			break;

		case '*':

			push(arv1 * arv2);
			break;

		case '/':

			push(arv1 / arv2);
			break;

		default:

			throw new RuntimeException("Avaldis sisaldab tundmatud tehtemärki " + op
					+ ". Kasutatavad tehtemärgid on aga +, -, *, / (OP meetod).");

		}
	}

	public long tos() {
		// TODO!!! Your code here!

		if (magasin.size() < 1)

			throw new RuntimeException("Avaldis on tühi ehk puuduvad nii arvud kui ka tehtemärgid (TOS meetod).");

		return magasin.getFirst();
	}

	@Override
	public boolean equals(Object o) {
		// TODO!!! Your code here!

		if (magasin.equals(((LongStack) o).magasin)) {

			return true;
		}

		else {

			return false;
		}
	}

	@Override
	public String toString() {
		// TODO!!! Your code here!

		if (stEmpty()) {

			return "";
		}

		StringBuffer suurendaja = new StringBuffer();

		for (int i = magasin.size() - 1; i >= 0; i--) {

			suurendaja.append(

					String.valueOf(magasin.get(i)) + " ");
		}

		return suurendaja.toString();
	}

	public static long interpret(String pol) {
		// TODO!!! Your code here!

		if (pol == null | pol.length() == 0 | pol == "") {

			throw new RuntimeException("Te sisestasite täiesti tühja avaldise (INTERPRET meetod).");
		}

		LongStack magasin = new LongStack();

		String[] osad = pol.trim().split("\\s+");

		int i = 0, operaator = 0;

		for (String osa : osad) {

			try {

				magasin.push(Integer.valueOf(osa));

				i++;
			}

			catch (NumberFormatException e) {

				if (magasin.stEmpty()) {

					throw new RuntimeException("Teie avaldis ei sisalda ühtegi arvu (INTERPRET meetod).");
				}

				else if (!osa.equals("+") && !osa.equals("-") && !osa.equals("*") && !osa.equals("/")) {

					throw new RuntimeException("Avaldis " + pol + " sisaldab tundmatut tehtemärki " + "'" + osa + "'" + " (INTERPRET meetod).");
				}

				else if (magasin.magasin.size() < 2) {

					throw new RuntimeException(
							"\n Vigane avaldis " + pol + " on tingitud ühest järgnevast asjaolust:"
							+ "\n 1) Tehtemärke on liiga palju (INTERPRET meetod)."
							+ "\n 2) Enne esimest tehtemärki pole vähemalt kahte arvu (INTERPRET meetod).");
				}

				magasin.op(osa);
				operaator++;
			}
		}

		if (i - 1 != operaator) {

			throw new RuntimeException("Tehtemärke on liiga vähe. Avaldise " + "'" + pol
					+ "'" + " tulemusena tekkis üleliigseid elemente (INTERPRET meetod).");
		}

		return magasin.pop();
	}

}
